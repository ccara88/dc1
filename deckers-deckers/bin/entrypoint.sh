#!/bin/sh

# set password

adduser -S -h /home/${DECKERS_USER} -s /bin/bash ${DECKERS_USER}
echo ${DECKERS_USER}:${DECKERS_HASH} | chpasswd -e

unset DECKERS_USER
unset DECKERS_HASH

/usr/local/bin/node app.js -p ${DECKERS_PORT}
